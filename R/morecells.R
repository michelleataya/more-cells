guacamaya<-colorRampPalette(c("blue", "#20B2AA", "#FFC125", "#ff4500", "#b22222"))
col_redblue<- rev(colorRampPalette(c("#67001F", "#B2182B", "#D6604D", "#F4A582",
                              "#FDDBC7", "#FFFFFF", "#D1E5F0", "#92C5DE",
                              "#4393C3", "#2166AC", "#053061"))(200))

scale.col<- function(color=guacamaya(10)){
  data.color<-data.frame(color, c(1:10))
  g<-ggplot(data.color, aes(x=c.1.10., y=1))+
    annotation_raster(t(as.matrix(color)), -Inf, Inf, -Inf, Inf, interpolate = TRUE)+
    scale_fill_manual(values=color)+scale_x_discrete(breaks=data.color$color)+
    guides(fill=FALSE)+
    labs(x="", y="")+
    theme(axis.line=element_blank(), axis.text=element_blank(), axis.ticks=element_blank(), panel.background = element_blank())
  return(g)
}

tsne.exprs<-function(paleta_col=guacamaya, rtsne_geo=rtsne_out$Y, mode="h+d", data_int=data, title=T){
  position<-rtsne_geo
  plot.list<-list()
  for (marker in 1:length(colnames(data_int))){
    print(colnames(data_int)[marker])
    ii <- cut(data_int[,marker], breaks = seq(min(data_int[,marker]), max(data_int[,marker]), len = length(data_int[,marker])), include.lowest = TRUE)
    paleta<-paleta_col(length(data_int[,marker]))[ii]
    
    g<-ggplot(as.data.frame(position), aes(x=V1, y=V2))+
      geom_point(colour=paleta, size=0.5)+
      labs(x="",y="")+
      # lims(y=c(-50,60), x=c(-60,40))+
      theme_bw()+
      theme(axis.text = element_blank(),
            axis.ticks = element_blank(),
            panel.grid = element_blank(),
            plot.margin = unit(c(0,0,0,0), "lines"))+
      theme(aspect.ratio = 1)
    if (mode == "h+d"){
      h<-ggplot(as.data.frame(data_int), aes(x=as.data.frame(data_int)[,marker]))+
        geom_density()+
        labs(x="",y="")+
        theme_classic()+
        theme(axis.text = element_blank(),
              axis.ticks = element_blank(),
              axis.line = element_blank(),
              plot.margin = unit(c(0,0,-1,0), "lines"))
      if (title==T){
        a<-grid.arrange(h,g, ncol=1, widths=1, respect=TRUE, heights=c(1/3,1), 
                      top=colnames(data_int)[marker], padding=unit(0,"line"))
      }else
      {
        a<-grid.arrange(h,g, ncol=1, widths=1, respect=TRUE, heights=c(1/3,1), 
                        padding=unit(0,"line"))
      }
    }
    if (mode == "d"){
      if (title==T){
        g<-g+ggtitle(label=colnames(data_int)[marker]) 
      }
      a<-g
    }
    plot.list[[marker]]<-a
  }
  return(plot.list)
}

tsne.depur<-function(data2=data, perc=1){
  for (num in 1:length(colnames(data2))){
    min_decil<-quantile(data2[,num], prob=seq(0,1, length=1001))[perc+1][[1]]
    max_decil<-quantile(data2[,num], prob=seq(0,1, length=1001))[1000-perc][[1]]
    data2[,num][data2[,num] < min_decil]<- min_decil
    data2[,num][data2[,num] > max_decil]<- max_decil
  }
  return(data2)
}

tsne.hist.display<-function(data2=data){
  plot.list<-list()
  for (marker in 1:length(colnames(data))){
    print(colnames(data)[marker])
    h<-ggplot(as.data.frame(data), aes(x=as.data.frame(data)[,marker]))+
      geom_density()+
      labs(x="",y="")+
      ggtitle(colnames(data)[marker])
      theme_classic()
      # theme(axis.text = element_blank(),
            # axis.ticks = element_blank(),
            # axis.line = element_blank(),
            # plot.margin = unit(c(0,0,-1,0), "lines"))
    plot.list[[marker]]<-ggplotGrob(h)
  }
  do.call(grid.arrange, plot.list)
}

tsne.multi<-function(type_group=types, donors=donor, rtsne_geo=rtsne_out$Y, mode="p+d", bins=75, title=T, legend_hex=T, data2=data){
  
  don_list<-unique(donors)
  min.list<-vector()
  min.type<-vector()
  for (type in type_group){
    min.group.list<-vector()
    for (type.group in type){
      min.group.list<-append(min.group.list,nrow(data2[donors == unique(donors)[type.group],]))
    }
    min.type<-append(min.type, min(min.group.list))
    min_local<-length(type)*min(min.group.list)
    min.list<-append(min.list,min_local)
  }
  plot.list<-list()
  cont<-1
  for (group in type_group){
    rtsne_group<-matrix(ncol=2, nrow=0)
    for (don in group){
      temp<-rtsne_geo[donors == don_list[don],]
      # set.seed(100)
      temp<-temp[sample(1:nrow(temp), min.type[cont]),]
      rtsne_group<-rbind(rtsne_group, temp)
    }
    # set.seed(100)
    rtsne_group <- rtsne_group[sample(1:nrow(rtsne_group), min(min.list)), ]
    print(nrow(rtsne_group))
    g<-ggplot(as.data.frame(rtsne_group), aes(x=V1, y=V2))+
      geom_point(data=as.data.frame(rtsne_geo), aes(x=V1, y=V2), size=0.5, shape=21,color="grey80", fill="grey80")+
      labs(x="",y="")+
      theme_bw()+
      theme(axis.text = element_blank(),
            axis.ticks = element_blank(),
            panel.grid = element_blank())+
      theme(aspect.ratio = 1)
    if (title == T){
      g<-g+ggtitle(label=names(type_group)[cont])
    }
    if (mode=="d"){
      g<-g+geom_density_2d()+
        stat_density_2d(aes(fill=..level.., alpha=..level..), geom="polygon", show.legend = FALSE)+
        scale_fill_gradient(low="blue", high="yellow")
    }
    if (mode=="p+d"){
      g<-g+geom_point(size=0.5, color="black")+
        geom_density_2d()+
        stat_density_2d(aes(fill=..level.., alpha=..level..), geom="polygon", show.legend = FALSE)+
        scale_fill_gradient(low="blue", high="yellow")
    }
    if (mode=="p"){
      g<-g+geom_point(size=0.5, color="black")
    }
    if (mode=="h"){
      g<-g+geom_hex(bins=bins)+
        scale_fill_gradientn(colors=c("black", "green", "yellow", "red"), limits=c(1,10))  
      if (legend_hex == F){
        g<-g+guides(fill=F)
      }
    }

    plot.list[[cont]]<-g
    cont<-cont+1

  }
  return(plot.list)
}
tsne.individual<-function(rtsne_geo=rtsne_out$Y,donors=donor, mode="p+d", title=T, bins=75, legend_hex=T){
  donor_list<-unique(donors)
  plot.list<-list()
  cont<-1
  for (don in donor_list){
    rtsne_don<-rtsne_geo[donors == don,]
    g<-ggplot(as.data.frame(rtsne_don), aes(x=V1, y=V2))+
      geom_point(data=as.data.frame(rtsne_geo), aes(x=V1, y=V2), size=0.5, shape=21,color="grey80", fill="grey80")+
      labs(x="",y="")+
      theme_bw()+
      theme(axis.text = element_blank(),
            axis.ticks = element_blank(),
            panel.grid = element_blank())+
      theme(aspect.ratio = 1)
    if (title == T){
      g<-g+ggtitle(label=don)
    }
    if (mode=="d"){
      g<-g+geom_density_2d()+
        stat_density_2d(aes(fill=..level.., alpha=..level..), geom="polygon", show.legend = FALSE)+
        scale_fill_gradient(low="blue", high="yellow")
    }
    if (mode=="p+d"){
      g<-g+geom_point(size=0.5, color="black")+
        geom_density_2d()+
        stat_density_2d(aes(fill=..level.., alpha=..level..), geom="polygon", show.legend = FALSE)+
        scale_fill_gradient(low="blue", high="yellow")
    }
    if (mode=="p"){
      g<-g+geom_point(size=0.5, color="black")
    }
    if (mode=="h"){
      g<-g+geom_hex(bins=bins)+
        scale_fill_gradientn(colors=c("black", "green", "yellow", "red"), limits=c(1,10))
      if (legend_hex == F){
        g<-g+guides(fill=F)
      }
    }
    
    plot.list[[cont]]<-g
    cont<-cont+1
  }
  return(plot.list)
}

tsne.heatmap<-function(data_int=data, npar_int=npar, order_manual=F, order=F, colors_manual=NULL){
  data2<-matrix(nrow=0, ncol=npar_int+1)
  nclust<-length(unique(data_int[,npar+1]))
  for (elem in c(1:nclust)){
    temp<-data_int[data_int[,npar_int+1] == elem,]
    data2<-rbind(data2, cbind(t(as.data.frame(apply(temp[,1:npar_int], 2, mean))), temp[1,(npar_int+1):ncol(temp)]))
  }
  rownames(data2)<-NULL

  for (num in 1:npar_int){
    data2[,num]<-data2[,num]-min(data2[,num])
    data2[,num]<-data2[,num]*100/max(data2[,num])
  }

  if (order_manual ==F ){
    heatmap.2(as.matrix(data2[,1:npar_int]), labRow=data2[, npar_int+1],trace="none", dendrogram = "none", key=FALSE,
            col=col_redblue,margins=c(7,5), lhei=c(0.25,3.75), lwid = c(0.25, 3.75))
  }
  if (order_manual == T){
    heatmap.2(as.matrix(data2[order,1:npar_int]), Rowv = NULL, labRow=data2[order, npar_int+1],trace="none", dendrogram = "none", key=FALSE,
              col=col_redblue,margins=c(7,5), lhei=c(0.25,3.75), lwid = c(0.25, 3.75), RowSideColors = colors_manual)
  }

}

tsne.clust2matrix<-function(data_int=data, types_group="none", donors=donor){
  donor_list<-unique(donors)
  nclust<-length(unique(data_int[,ncol(data_int)]))
  clust_matrix<-matrix(nrow=length(donor_list), ncol=nclust)
  for (don in 0:(length(donor_list)-1)){
    for (clust in 1:nclust){
      tdonor<-donor_list[don+1]
      data_temp<-data[which(donor == tdonor),]
      perc<-length(data_temp[,ncol(data_int)][data_temp[,ncol(data_int)] == clust])*100/length(data_temp[,ncol(data_int)])
      clust_matrix[don+1, clust]<-perc
    }
  }
  colnames(clust_matrix)<-sapply(1:nclust, function(x) paste("CLUST",x, sep="_"))
  rownames(clust_matrix)<-donor_list
  
  if (types_group != "none"){
    clust_matrix<-as.data.frame(clust_matrix)
    clust_matrix[nclust+1]<-rep(NA, length(donor_list))
    for (num in 1:length(names(types_group))){
      clust_matrix[types[[names(types_group)[num]]],ncol(clust_matrix)]<-names(types_group)[num]
    }
    colnames(clust_matrix)[nclust+1]<-"Type"
  }
  return(clust_matrix)
}

tsne.sample<-function(sample, rep=20, subset){
  if (subset < nrow(sample)){
    rtsne_group <- sample
    group.list <- list()                                                                                                                                                                                   
    rep<-rep
    subset<-subset
    for (num in 1:rep){
      rtsne_group_sub <- rtsne_group[sample(1:nrow(rtsne_group), subset), ]
      group.list[[num]] <- rtsne_group_sub
    }
    group.mat <- matrix(nrow=subset*2, ncol=rep)
    for (num in 1:length(group.list)){
      m<- group.list[[num]]
      m<-m[order(m[,1]),]
      group.mat[,num]<-as.vector(unlist(m))
    }
    t<-dist(t(group.mat))
    min_dist<-apply(as.matrix(t), 2, sum)
    min_subset<-as.numeric(which(min_dist == min(min_dist)))
    return(group.list[[min_subset]])
  }else{
    return(sample)
  }
}

tsne.pairplot<- function(matrix, guides=T, title=F, text=F){
  edge_test<-as.data.frame(matrix(ncol=3, nrow=0))
  for (col in 1:ncol(matrix)){
    for (row in 1:nrow(matrix)){
      edge_test<-rbind(edge_test, as.data.frame(matrix(c(colnames(matrix)[col], rownames(matrix)[row], matrix[row,col]), nrow=1)))
    }
  }
  edge_test$V1<-as.factor(edge_test$V1)
  edge_test$V2<-as.factor(edge_test$V2)
  edge_test<-edge_test[!is.na(edge_test$V3),]
  edge_test$V3<-as.numeric(as.character(edge_test$V3))
  edge_test$V4<-factor(0,levels=c(0,1))
  edge_test$V4[edge_test$V3 < 0.05]<-as.factor(0)
  edge_test$V4[edge_test$V3 >= 0.05]<-as.factor(1)
  edge_test$V5<-edge_test$V3
  edge_test$V5[edge_test$V3 < 0.05 & edge_test$V3 >= 0.01]<-"*"
  edge_test$V5[edge_test$V3 < 0.01 & edge_test$V3 >= 0.001]<-"**"
  edge_test$V5[edge_test$V3 < 0.001]<-"***"
  edge_test$V5[edge_test$V3 >= 0.05]<-""
  g<-ggplot(edge_test, aes(V1, V2))+
    geom_tile(aes(fill=as.factor(V4)), color="black")+
    scale_fill_manual(values=c("black","white"), labels=c("p < 0.05", "p >= 0.05"), drop=FALSE)+
    labs(fill="Significance", x="",y="")+
    scale_x_discrete(limits=unique(edge_test$V1))+
    scale_y_discrete(limits=rev(unique(edge_test$V2)))+
    theme_minimal()+
    theme(axis.text.x=element_text(angle=45, hjust=1), aspect.ratio = 1)
  if (guides == F){
    g<-g+guides(fill=F)
  }
  if (title != F){
    g<-g+ggtitle(title)
  }
  if (text == "text"){
    g<-g+geom_text(aes(label=round(edge_test$V3, 3), colour=as.factor(edge_test$V4)), size=2)+
      scale_colour_manual(values=c("yellow", "black"))+
      guides(color=F)
  }
  if (text == "text"){
    g<-g+geom_text(aes(label=round(edge_test$V3, 3), colour=as.factor(edge_test$V4)), size=2)+
      scale_colour_manual(values=c("yellow", "black"), drop=F)+
      guides(color=F)
  }
  if (text == "symbol"){
    g<-g+geom_text(aes(label=V5), colour="yellow", size=4)
  }
  return(g)
}

tsne.expr_matrix<-function(par, par_ind, population, gs_int=gs, cutoff=1000, seed=123){
    npar<-length(par)
    data_total<-matrix(nrow = 0, ncol=npar)
    donor<-vector()
    for (num in 1:length(sampleNames(gs_int))){
      print(num)
      print(sampleNames(gs_int[[num]]))
      data <- flowCore::exprs(getData(gs_int[[num]], population))
      colnames(data)[par_ind]<-par
      colnames_proj <- unname(colnames(data))[par_ind]
      
      if (nrow(data) < cutoff) {
        nsub <- nrow(data)
      }else{
        nsub <- cutoff
      }
      if (!is.null(seed)){
        set.seed(seed)
    } 
      data <- data[sample(1:nrow(data), nsub), ]
      data <- data[, colnames_proj]
      data <- data[!duplicated(data),]
      data_total<-rbind(data_total, data)
      donor<-append(donor, rep(sampleNames(gs_int)[num], length(data[,1])))
    }
    return(list(data_total, donor))
}
