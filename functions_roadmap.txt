col_redblue.Rd
guacamaya.Rd
scale.col.Rd
tsne.individual.Rd
tsne.pairplot.Rd
tsne.sample.Rd

Revision:
tsne.hist.display.Rd
tsne.clust2matrix.Rd
tsne.heatmap.Rd

Done:
morecells-package.Rd
tsne.expr_matrix.Rd
tsne.exprs.Rd
tsne.depur.Rd
tsne.multi.Rd
